"""
Functions performing some matrix operations.
"""
__author__ = "Breogan Costa (GitLab/Hub: covelus)"

import numpy as np  # NumPy scientific numeric library
import inspect  # standard Python introspection package


def do_noting1(array, verbose=False):
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[:] or arr")
    arr = arr[:]
    return arr


def do_noting2(array, verbose=False):
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[::] or arr")
    arr = arr[::]
    return arr


def do_noting3(array, verbose=False):
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[::1] or arr")
    arr = arr[::1]
    return arr


def get_last_column_as_1d_vector(array, verbose=False):
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[:,-1] or arr")
    arr = arr[:, -1]
    return arr


def get_last_column_as_2d_matrix(array, verbose=False):
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[:,-1:] or arr")
    arr = arr[:, -1:]
    return arr


def get_view_invert_rows_order(array, verbose=False):
    # Supporting reading: https://stackoverflow.com/questions/6771428/most-efficient-way-to-reverse-a-numpy-array
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[::-1] or arr")
    arr = arr[::-1]
    return arr


def get_view_invert_cols_order(array, verbose=False):
    # Supporting reading: https://stackoverflow.com/questions/49545758/flip-or-reverse-columns-in-numpy-array
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[:,::-1] or arr")
    arr = arr[:, ::-1]
    return arr


def get_view_invert_rows_cols_order(array, verbose=False):
    # Supporting reading: https://stackoverflow.com/questions/49545758/flip-or-reverse-columns-in-numpy-array
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[::-1,::-1] or arr")
    arr = arr[::-1, ::-1]
    return arr


def get_last_row_1d(array, verbose=False):
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[-1] or arr")
    arr = arr[-1]
    return arr


def get_last_row_2d(array, verbose=False):
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[-1:] or arr")
    arr = arr[-1:]
    return arr


def remove_last_row1(array, verbose=False):
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[:-1] or arr")
    arr = arr[:-1]
    return arr


def remove_last_row2(array, verbose=False):
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[:-1:] or arr")
    arr = arr[:-1:]
    return arr


def remove_last_2_rows(array, verbose=False):
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[:-2:] or arr")
    arr = arr[:-2:]
    return arr


def get_first_3_rows(array, verbose=False):
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[:3:] or arr")
    arr = arr[:3:]
    return arr


def remove_first_2_rows(array, verbose=False):
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[2::] or arr")
    arr = arr[2::]
    return arr


def test_placeholder_for_next_experiment(array, verbose=False):
    arr = np.copy(array)
    frame = inspect.currentframe()
    if verbose:
        print(f"{frame.f_code.co_name}: Performing arr[::] or arr")
    arr = arr[::]
    return arr
