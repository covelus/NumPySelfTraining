"""
Experiments file.
"""
__author__ = "Breogan Costa (GitLab/Hub: covelus)"

import numpy as np  # NumPy scientific numeric library
from matrix_ops import *

VERBOSE_ENABLED = True


def create_matrix_2d_0_to_24():
    npa = np.arange(0, 25, dtype=int)  # generates a 1D vector (ndarray) with 25 sequential integer starting at 0
    npa = np.reshape(npa, (5, 5))  # converts (changes shape) the 1D vector in a 5x5 2D Matrix (ndarray)
    return npa


def do_matrices_experiment():
    print(f"NumPy Self Training, this was done when available v. 1.23.3. Current installed version: {np.__version__ }")
    arr_def = create_matrix_2d_0_to_24()
    print(f"Original Matrix: \n{str(arr_def)}")

    print(do_noting1(arr_def, VERBOSE_ENABLED))

    print(do_noting2(arr_def, VERBOSE_ENABLED))

    print(do_noting3(arr_def, VERBOSE_ENABLED))

    print(get_last_column_as_1d_vector(arr_def, VERBOSE_ENABLED))

    print(get_last_column_as_2d_matrix(arr_def, VERBOSE_ENABLED))

    print(get_view_invert_rows_order(arr_def, VERBOSE_ENABLED))

    print(get_view_invert_cols_order(arr_def, VERBOSE_ENABLED))

    print(get_view_invert_rows_cols_order(arr_def, VERBOSE_ENABLED))

    print(get_last_row_1d(arr_def, VERBOSE_ENABLED))

    print(get_last_row_2d(arr_def, VERBOSE_ENABLED))

    print(remove_last_row1(arr_def, VERBOSE_ENABLED))

    print(remove_last_row2(arr_def, VERBOSE_ENABLED))

    print(remove_last_2_rows(arr_def, VERBOSE_ENABLED))

    print(get_first_3_rows(arr_def, VERBOSE_ENABLED))

    print(remove_first_2_rows(arr_def, VERBOSE_ENABLED))

    print(test_placeholder_for_next_experiment(arr_def, VERBOSE_ENABLED))
