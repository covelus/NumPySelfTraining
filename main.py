#!/usr/bin/env python3
"""
Main module for the self learning exercise to solve NumPy doubts related to Matrices (Numpy's arrays: ndarrays).
Some notes might seem to basic, but the intention is to try to stay didactic, so, this could be shared as notes for
NumPy beginners.

Usage:
    python3 main.py
Dependencies:
    NumPy package.
        Install it with:
            pip install numpy
            pip3 install numpy
"""
__author__ = "Breogan Costa (GitLab/Hub: covelus)"

from experiment_matrices import do_matrices_experiment


def crate_matrix():
    npa = np.arange(0, 25, dtype=int)  # generates a 1D vector (ndarray) with 25 integer numbers starting at 0 [0..24]
    npa = np.reshape(npa,  (5, 5))  # converts (changes shape) the 1D vector (ndarray) in a 5x5 2D Matrix (ndarray)
    return npa


if __name__ == '__main__':
    do_matrices_experiment()
