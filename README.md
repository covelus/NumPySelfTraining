# NumPySelfTraining


## Objective

Just NumPy experiments to improve my knowledge.

## Dependencies

* Python 3.10 or newer
* NumPy 1.23.3 or newer. Install it by running:

    
    pip install numpy  
    # or
    pip3 install numpy

## Usage:
    python3 main.py
